"use strict";

let numb = prompt("Enter a number");

if (isNaN(+numb) || numb == null || !!(+numb - Math.floor(+numb))) {
    while (isNaN(+numb) || numb == null || !!(+numb - Math.floor(+numb))) {
        numb = prompt("Enter a number");
    }
}

if (+numb < 5 && +numb > -5) {
    console.log("Sorry, no numbers");
} else if (+numb < 0) {
    for (let i = 0; i >= numb; i--) {
        if (i % 5 == 0) {
            console.log(i);
        }
    }
} else {
    for (let i = 0; i <= numb; i++) {
        if (i % 5 == 0) {
            console.log(i);
        }
    }
}
